# Copyright 2023 Raphaël Biojout <https://www.belgalli.com>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    "name": "Attachment Zip & Pdf Download",
    "version": "15.0.1.0",
    "category": "Tools",
    'description': """
    Attachment Zip & Pdf Download module.
    ========================
    This module allow to consolidate and download Zip and Pdf from attachments.
    """,
    "website": "https://www.belgalli.com",
    'images': ['static/description/banner.png'],
    'author': "contact@belgalli.com",
    "license": "AGPL-3",
    "depends": ["base"],
    "external_dependencies": {"python": ["img2pdf"]},
    "data": [
        "views/ir_attachment_view.xml",
    ],
    "installable": True,
    'price': 4.99,
    'currency': 'EUR',
}
