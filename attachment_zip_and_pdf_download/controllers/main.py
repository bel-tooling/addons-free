# Copyright 2023 Raphaël Biojout <https://www.belgalli.com>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

from odoo import _, http
from odoo.http import request


class AttachmentZipAndPdfDownloadController(http.Controller):
    @http.route("/web/attachment/download_zip", type="http", auth="user")
    def download_zip(self, ids=None, debug=0):
        ids = [] if not ids else ids
        if len(ids) == 0:
            return
        list_ids = map(int, ids.split(","))
        out_file = request.env["ir.attachment"].sudo().browse(list_ids)._create_consolidated_zip()
        return http.send_file(
            filepath_or_fp=out_file,
            mimetype="application/zip",
            as_attachment=True,
            filename=_("attachments.zip"),
        )

    @http.route("/web/attachment/consolidate_pdf", type="http", auth="user")
    def consolidate_pdf(self, ids=None, debug=0):
        ids = [] if not ids else ids
        if len(ids) == 0:
            return
        list_ids = map(int, ids.split(","))
        out_file = request.env["ir.attachment"].browse(list_ids)._create_consolidated_pdf()
        return http.send_file(
            filepath_or_fp=out_file,
            mimetype="application/pdf",
            as_attachment=True,
            filename=_("attachments.pdf"),
        )
