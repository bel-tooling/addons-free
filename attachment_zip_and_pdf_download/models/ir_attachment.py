# Copyright 2023 Raphaël Biojout <https://www.belgalli.com>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
import zipfile
from io import BytesIO

import img2pdf

from odoo import _, models
from odoo.exceptions import UserError

from odoo.tools import pdf
import io


class IrAttachment(models.Model):
    _inherit = "ir.attachment"

    def action_attachments_download(self):
        items = self.filtered(lambda x: x.type == "binary")
        if not items:
            raise UserError(
                _("None attachment selected. Only binary attachments allowed.")
            )
        ids = ",".join(map(str, items.ids))
        return {
            "type": "ir.actions.act_url",
            "url": "/web/attachment/download_zip?ids=%s" % (ids),
            "target": "self",
        }

    def _create_consolidated_zip(self):
        zip_buffer = BytesIO()
        with zipfile.ZipFile(zip_buffer, "a", zipfile.ZIP_DEFLATED, False) as zip_file:
            for attachment in self:
                # CASE 1 : we have the data
                if len(attachment.raw) > 0:
                    zip_file.writestr(
                        str(attachment.id) + "_" + attachment.name,  # File to replace with care of uniqueness
                        attachment.raw  # Data
                    )
                # CASE 2 : we have a path
                else:
                    try:
                        zip_file.write(attachment._full_path(attachment.store_fname), attachment.name)
                    except FileNotFoundError:
                        continue
            zip_buffer.seek(0)
            zip_file.close()
        return zip_buffer

    def action_attachments_consolidated_pdf(self):
        items = self.filtered(lambda x: x.type == "binary")
        if not items:
            raise UserError(
                _("None attachment selected. Only binary attachments allowed.")
            )
        ids = ",".join(map(str, items.ids))
        return {
            "type": "ir.actions.act_url",
            "url": "/web/attachment/consolidate_pdf?ids=%s" % (ids),
            "target": "self",
        }

    def _create_consolidated_pdf(self):
        # START with PDF and ADD images
        merged_pdf = pdf.merge_pdf(
            [attachment.raw for attachment in self if attachment.mimetype.lower() == 'application/pdf'] +
            [img2pdf.convert(attachment.raw) for attachment in self
             if attachment.mimetype.lower().startswith('image/')])

        merged_reader_buffer = io.BytesIO(merged_pdf)
        return merged_reader_buffer
