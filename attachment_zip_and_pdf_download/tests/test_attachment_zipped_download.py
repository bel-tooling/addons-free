# Copyright 2023 Raphaël Biojout <https://www.belgalli.com>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
import base64

from odoo import http

import odoo.tests


class TestAttachmentZippedDownload(odoo.tests.HttpCase):
    def setUp(self):
        super().setUp()
        test_1 = self._create_attachment("test1.txt")
        test_2 = self._create_attachment("test2.txt")
        self.attachments = test_1 + test_2
        self.user = self.env["res.users"].with_context(no_reset_password=True).create(
            {
                "name": "test-user",
                "login": "test-user",
                "password": "test-user",
                'signature': '--\nMark',
                'email': 'default_test_zipped@example.com',
                "groups_id": [(6, 0, [self.env.ref("base.group_user").id])],
            }
        )

    def _create_attachment(self, name):
        return self.env["ir.attachment"].create(
            {
                "name": name,
                "datas": base64.b64encode(b"\xff data"),
            }
        )

    # @TODO
    def test_action_attachments_download(self):
        self.authenticate("test-user", "test-user")
        res = self.attachments.action_attachments_download()
        response = self.url_open(res["url"], data={
            'token': 'dummy',
            'csrf_token': http.WebRequest.csrf_token(self)
        }, timeout=20)
        self.assertEqual(response.status_code, 200)
