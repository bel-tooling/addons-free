# Copyright 2022 Raphaël Biojout <https://www.belgalli.com>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    'name': 'Database Rebuild',
    'category': 'Tools',
    'version': '15.0.1.0',
    'description': """
Rebuild database module.
========================
This module help to rebuild the database by updating all modules with the password of the odoo.conf.
""",
    "website": "https://www.belgalli.com",
    'images': ['static/description/banner.png'],
    'author': "contact@belgalli.com",
    'license': 'AGPL-3',
    'depends': ['base', 'web'],
    'auto_install': True,
    'data': [
    ],
    'assets': {
    },
    'bootstrap': True,  # load translations for login screen,
    'price': 4.99,
    'currency': 'EUR',
}
