# Copyright 2022 Raphaël Biojout <https://www.belgalli.com>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
from lxml import html

import odoo
import odoo.modules.registry
from odoo.addons.base.models.ir_qweb import render as qweb_render
from odoo.tools.misc import file_open
from odoo import http
from odoo.http import dispatch_rpc, request

from odoo.addons.web.controllers.main import Database, DBNAME_PATTERN

import logging

_logger = logging.getLogger(__name__)

# ----------------------------------------------------------
# Odoo Web helpers
# ----------------------------------------------------------

db_list = http.db_list

db_monodb = http.db_monodb


class DatabaseExtended(Database):

    def _render_template(self, **d):
        d.setdefault('manage', True)
        d['insecure'] = odoo.tools.config.verify_admin_password('admin')
        d['list_db'] = odoo.tools.config['list_db']
        d['langs'] = odoo.service.db.exp_list_lang()
        d['countries'] = odoo.service.db.exp_list_countries()
        d['pattern'] = DBNAME_PATTERN
        # databases list
        d['databases'] = []
        try:
            d['databases'] = http.db_list()
            d['incompatible_databases'] = odoo.service.db.list_db_incompatible(d['databases'])
        except odoo.exceptions.AccessDenied:
            monodb = db_monodb()
            if monodb:
                d['databases'] = [monodb]

        templates = {}

        with file_open("database_rebuild/static/src/public/database_manager.qweb.html", "r") as fd:
            template = fd.read()
        with file_open("web/static/src/public/database_manager.master_input.qweb.html", "r") as fd:
            templates['master_input'] = fd.read()
        with file_open("web/static/src/public/database_manager.create_form.qweb.html", "r") as fd:
            templates['create_form'] = fd.read()

        def load(template_name, options):
            return (html.fragment_fromstring(templates[template_name]), template_name)

        return qweb_render(html.document_fromstring(template), d, load=load)

    def _render_template_rebuild(self, **d):
        d.setdefault('manage', True)
        d['insecure'] = odoo.tools.config.verify_admin_password('admin')
        d['list_db'] = odoo.tools.config['list_db']
        # databases list
        d['databases'] = []

        try:
            d['databases'] = http.db_list()
            d['incompatible_databases'] = odoo.service.db.list_db_incompatible(d['databases'])
        except odoo.exceptions.AccessDenied:
            monodb = db_monodb()
            if monodb:
                d['databases'] = [monodb]

        templates = {}

        with file_open("database_rebuild/static/src/public/database_rebuild.qweb.html", "r") as fd:
            template = fd.read()
        with file_open("web/static/src/public/database_manager.master_input.qweb.html", "r") as fd:
            templates['master_input'] = fd.read()

        def load(template_name, options):
            return (html.fragment_fromstring(templates[template_name]), template_name)

        return qweb_render(html.document_fromstring(template), values=d, load=load)

    @http.route('/web/database/rebuild', type='http', auth="none")
    def rebuild(self, **kw):
        request._cr = None
        return self._render_template_rebuild()

    def registry(self, name, new=False, **kwargs):
        self.ensure_one()
        m = odoo.modules.registry.Registry
        return m.new(self.name, **kwargs)

    @http.route('/web/database/request/rebuild', type='http', auth="none", methods=['POST'], csrf=False)
    def request_rebuild(self, master_pwd, name, **post):
        insecure = odoo.tools.config.verify_admin_password('admin')
        if insecure and master_pwd:
            dispatch_rpc('db', 'change_admin_password', ["admin", master_pwd])
        try:
            odoo.service.db.check_super(master_pwd)
            odoo.tools.config['update']['base'] = True
            odoo.modules.registry.Registry.new(name, force_demo=False, update_module=True)
            return request.redirect('/web/database/rebuild')
        except Exception as e:
            error = "Database rebuild error: %s" % (str(e) or repr(e))
            return self._render_template_rebuild(error=error)
